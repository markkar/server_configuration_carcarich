-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Dec 01, 2021 at 10:34 AM
-- Server version: 5.7.31
-- PHP Version: 7.3.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `autoshop_db`
--
CREATE DATABASE IF NOT EXISTS `autoshop_db` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `autoshop_db`;

-- --------------------------------------------------------

--
-- Table structure for table `cars`
--

DROP TABLE IF EXISTS `cars`;
CREATE TABLE IF NOT EXISTS `cars` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `number` varchar(15) NOT NULL,
  `client_id` int(11) DEFAULT NULL,
  `car_type_id` int(11) NOT NULL,
  `engine_power` decimal(7,2) NOT NULL,
  `fuel_cons` decimal(5,2) NOT NULL,
  `trunk_volume` smallint(6) DEFAULT NULL,
  `color` varchar(20) NOT NULL,
  `vin` varchar(17) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `car_types`
--

DROP TABLE IF EXISTS `car_types`;
CREATE TABLE IF NOT EXISTS `car_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `brand` varchar(30) NOT NULL,
  `model` varchar(40) NOT NULL,
  `prod_year` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

DROP TABLE IF EXISTS `comments`;
CREATE TABLE IF NOT EXISTS `comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `prod_id` int(11) NOT NULL,
  `text` text NOT NULL,
  `mark` decimal(2,1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `prod_id` (`prod_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `entities`
--

DROP TABLE IF EXISTS `entities`;
CREATE TABLE IF NOT EXISTS `entities` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `legal_adress` varchar(60) NOT NULL,
  `tax_number` varchar(40) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `entities_branches`
--

DROP TABLE IF EXISTS `entities_branches`;
CREATE TABLE IF NOT EXISTS `entities_branches` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `entity_id` int(11) NOT NULL,
  `region` varchar(40) NOT NULL,
  `city` varchar(40) NOT NULL,
  `street` varchar(30) NOT NULL,
  `number` varchar(5) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `entity_id` (`entity_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `get_points`
--

DROP TABLE IF EXISTS `get_points`;
CREATE TABLE IF NOT EXISTS `get_points` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `region` varchar(40) NOT NULL,
  `city` varchar(40) NOT NULL,
  `street` varchar(30) NOT NULL,
  `number` varchar(5) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

DROP TABLE IF EXISTS `orders`;
CREATE TABLE IF NOT EXISTS `orders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `get_pointID` int(11) NOT NULL,
  `status` enum('basket','in_proccess','ready') NOT NULL,
  `order_date` date NOT NULL,
  `sum` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `prods`
--

DROP TABLE IF EXISTS `prods`;
CREATE TABLE IF NOT EXISTS `prods` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `car_type_id` int(11) DEFAULT NULL,
  `is_original` tinyint(1) NOT NULL,
  `photo` varchar(40) DEFAULT NULL,
  `category` varchar(30) NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `car_type_id` (`car_type_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `prod_branch_order`
--

DROP TABLE IF EXISTS `prod_branch_order`;
CREATE TABLE IF NOT EXISTS `prod_branch_order` (
  `order_id` int(11) NOT NULL,
  `prod_id` int(11) NOT NULL,
  `branch_id` int(11) NOT NULL,
  `count` int(11) NOT NULL,
  PRIMARY KEY (`order_id`,`prod_id`,`branch_id`),
  KEY `prod_id` (`prod_id`),
  KEY `branch_id` (`branch_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `prod_entity`
--

DROP TABLE IF EXISTS `prod_entity`;
CREATE TABLE IF NOT EXISTS `prod_entity` (
  `prod_id` int(11) NOT NULL,
  `entity_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `cost` int(11) NOT NULL,
  PRIMARY KEY (`prod_id`,`entity_id`),
  KEY `entity_id` (`entity_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `login` varchar(30) NOT NULL,
  `password` varchar(40) NOT NULL,
  `hash` varchar(32) DEFAULT NULL,
  `second_name` varchar(30) NOT NULL,
  `first_name` varchar(30) NOT NULL,
  `city` varchar(40) NOT NULL,
  `status` enum('wholesale','retail') NOT NULL,
  `type` enum('phis','jur') NOT NULL,
  `date_of_birth` date NOT NULL,
  `email` varchar(40) NOT NULL,
  `phone` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `login`, `password`, `hash`, `second_name`, `first_name`, `city`, `status`, `type`, `date_of_birth`, `email`, `phone`) VALUES
(2, 'markkar', 'f675e0cf1fdd270c33d020dfecfbfec0', '05ab36fd93ea6f1128ec60413952189a', 'Karavashkin', 'Mark', 'Moscow', 'retail', 'phis', '2021-11-03', 'mark.kem01@gmail.com', '89095204213'),
(3, 'rrr', '656f697a7b0f56984e496ad586cbebdf', '76aaeeb140669d8874c98b5064324fec', 'Karavashkin', 'Mark', 'Moscow', 'wholesale', 'phis', '2021-11-03', 'mark.kem01@gmail.com', '89095204213'),
(4, 'Mark', 'f675e0cf1fdd270c33d020dfecfbfec0', 'd7320312e2b586c0b026733d12704a3c', 'Karavashkin', 'Mark', 'Moscow', 'retail', 'phis', '2021-11-03', 'mark.kem01@gmail.com', '89095204213');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
